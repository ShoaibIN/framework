<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ArtifactsCredits Controller
 *
 * @property \App\Model\Table\ArtifactsCreditsTable $ArtifactsCredits
 *
 * @method \App\Model\Entity\ArtifactsCredit[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsCreditsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Artifacts', 'Credits']
        ];
        $artifactsCredits = $this->paginate($this->ArtifactsCredits);

        $this->set(compact('artifactsCredits'));
    }

    /**
     * View method
     *
     * @param string|null $id Artifacts Credit id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactsCredit = $this->ArtifactsCredits->get($id, [
            'contain' => ['Artifacts', 'Credits']
        ]);

        $this->set('artifactsCredit', $artifactsCredit);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $artifactsCredit = $this->ArtifactsCredits->newEntity();
        if ($this->request->is('post')) {
            $artifactsCredit = $this->ArtifactsCredits->patchEntity($artifactsCredit, $this->request->getData());
            if ($this->ArtifactsCredits->save($artifactsCredit)) {
                $this->Flash->success(__('The artifacts credit has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts credit could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsCredits->Artifacts->find('list', ['limit' => 200]);
        $credits = $this->ArtifactsCredits->Credits->find('list', ['limit' => 200]);
        $this->set(compact('artifactsCredit', 'artifacts', 'credits'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Artifacts Credit id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $artifactsCredit = $this->ArtifactsCredits->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $artifactsCredit = $this->ArtifactsCredits->patchEntity($artifactsCredit, $this->request->getData());
            if ($this->ArtifactsCredits->save($artifactsCredit)) {
                $this->Flash->success(__('The artifacts credit has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts credit could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsCredits->Artifacts->find('list', ['limit' => 200]);
        $credits = $this->ArtifactsCredits->Credits->find('list', ['limit' => 200]);
        $this->set(compact('artifactsCredit', 'artifacts', 'credits'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Artifacts Credit id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $artifactsCredit = $this->ArtifactsCredits->get($id);
        if ($this->ArtifactsCredits->delete($artifactsCredit)) {
            $this->Flash->success(__('The artifacts credit has been deleted.'));
        } else {
            $this->Flash->error(__('The artifacts credit could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
