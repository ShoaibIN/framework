<?php
declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * SignReadingsComments Controller
 *
 * @property \App\Model\Table\SignReadingsCommentsTable $SignReadingsComments
 *
 * @method \App\Model\Entity\SignReadingsComment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SignReadingsCommentsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SignReadings', 'Authors'],
        ];
        $signReadingsComments = $this->paginate($this->SignReadingsComments);

        $this->set(compact('signReadingsComments'));
    }

    /**
     * View method
     *
     * @param string|null $id Sign Readings Comment id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $signReadingsComment = $this->SignReadingsComments->get($id, [
            'contain' => ['SignReadings', 'Authors'],
        ]);

        $this->set('signReadingsComment', $signReadingsComment);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $signReadingsComment = $this->SignReadingsComments->newEntity();
        if ($this->request->is('post')) {
            $signReadingsComment = $this->SignReadingsComments->patchEntity($signReadingsComment, $this->request->getData());
            if ($this->SignReadingsComments->save($signReadingsComment)) {
                $this->Flash->success(__('The sign readings comment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sign readings comment could not be saved. Please, try again.'));
        }
        $signReadings = $this->SignReadingsComments->SignReadings->find('list', ['limit' => 200]);
        $authors = $this->SignReadingsComments->Authors->find('list', ['limit' => 200]);
        $this->set(compact('signReadingsComment', 'signReadings', 'authors'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sign Readings Comment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $signReadingsComment = $this->SignReadingsComments->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $signReadingsComment = $this->SignReadingsComments->patchEntity($signReadingsComment, $this->request->getData());
            if ($this->SignReadingsComments->save($signReadingsComment)) {
                $this->Flash->success(__('The sign readings comment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sign readings comment could not be saved. Please, try again.'));
        }
        $signReadings = $this->SignReadingsComments->SignReadings->find('list', ['limit' => 200]);
        $authors = $this->SignReadingsComments->Authors->find('list', ['limit' => 200]);
        $this->set(compact('signReadingsComment', 'signReadings', 'authors'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Sign Readings Comment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $signReadingsComment = $this->SignReadingsComments->get($id);
        if ($this->SignReadingsComments->delete($signReadingsComment)) {
            $this->Flash->success(__('The sign readings comment has been deleted.'));
        } else {
            $this->Flash->error(__('The sign readings comment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

