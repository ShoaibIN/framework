<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Rulers Controller
 *
 * @property \App\Model\Table\RulersTable $Rulers
 *
 * @method \App\Model\Entity\Ruler[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RulersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Periods', 'Dynasties']
        ];
        $rulers = $this->paginate($this->Rulers);

        $this->set(compact('rulers'));
    }

    /**
     * View method
     *
     * @param string|null $id Ruler id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ruler = $this->Rulers->get($id, [
            'contain' => ['Periods', 'Dynasties', 'Dates']
        ]);

        $this->set('ruler', $ruler);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ruler = $this->Rulers->newEntity();
        if ($this->request->is('post')) {
            $ruler = $this->Rulers->patchEntity($ruler, $this->request->getData());
            if ($this->Rulers->save($ruler)) {
                $this->Flash->success(__('The ruler has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ruler could not be saved. Please, try again.'));
        }
        $periods = $this->Rulers->Periods->find('list', ['limit' => 200]);
        $dynasties = $this->Rulers->Dynasties->find('list', ['limit' => 200]);
        $this->set(compact('ruler', 'periods', 'dynasties'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Ruler id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ruler = $this->Rulers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ruler = $this->Rulers->patchEntity($ruler, $this->request->getData());
            if ($this->Rulers->save($ruler)) {
                $this->Flash->success(__('The ruler has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ruler could not be saved. Please, try again.'));
        }
        $periods = $this->Rulers->Periods->find('list', ['limit' => 200]);
        $dynasties = $this->Rulers->Dynasties->find('list', ['limit' => 200]);
        $this->set(compact('ruler', 'periods', 'dynasties'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Ruler id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ruler = $this->Rulers->get($id);
        if ($this->Rulers->delete($ruler)) {
            $this->Flash->success(__('The ruler has been deleted.'));
        } else {
            $this->Flash->error(__('The ruler could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
