<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsCredit[]|\Cake\Collection\CollectionInterface $artifactsCredits
 */
?>
<div class="artifactsCredits index large-9 medium-8 columns content">
    <h3><?= __('Artifacts Credits') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('credit_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('credit_type') ?></th>
                <!-- <th scope="col" class="actions"><?= __('Actions') ?></th> -->
            </tr>
        </thead>
        <tbody>
            <?php foreach ($artifactsCredits as $artifactsCredit): ?>
            <tr>
                <td><?= $artifactsCredit->has('artifact') ? $this->Html->link($artifactsCredit->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsCredit->artifact->id]) : '' ?></td>
                <td><?= $artifactsCredit->has('credit') ? $this->Html->link($artifactsCredit->credit->credit, ['controller' => 'Credits', 'action' => 'view', $artifactsCredit->credit->id]) : '' ?></td>
                <td><?= h($artifactsCredit->date) ?></td>
                <td><?= h($artifactsCredit->credit_type) ?></td>
                <!-- <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $artifactsCredit->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $artifactsCredit->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $artifactsCredit->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsCredit->id)]) ?>
                </td> -->
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
