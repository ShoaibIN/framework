<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsPublication $artifactsPublication
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsPublication) ?>
            <legend class="capital-heading"><?= __('Edit Artifacts Publication') ?></legend>
            <?php
                echo $this->Form->control('artifact_id', ['options' => $artifacts]);
                echo $this->Form->control('publication_id', ['options' => $publications]);
                echo $this->Form->control('exact_reference');
                echo $this->Form->control('publication_type');
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $artifactsPublication->id],
                ['class' => 'btn-action'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsPublication->id)]
            )
        ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts Publications'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
