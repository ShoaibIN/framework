<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Material Entity
 *
 * @property int $id
 * @property string $material
 * @property int|null $parent_id
 *
 * @property \App\Model\Entity\Material $parent_material
 * @property \App\Model\Entity\Material[] $child_materials
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class Material extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'material' => true,
        'parent_id' => true,
        'parent_material' => true,
        'child_materials' => true,
        'artifacts' => true
    ];
}
