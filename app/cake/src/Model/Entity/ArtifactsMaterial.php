<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactsMaterial Entity
 *
 * @property int $id
 * @property int $artifact_id
 * @property int $material_id
 * @property bool|null $is_material_uncertain
 * @property int|null $material_color_id
 * @property int|null $material_aspect_id
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\Material $material
 * @property \App\Model\Entity\MaterialColor $material_color
 * @property \App\Model\Entity\MaterialAspect $material_aspect
 */
class ArtifactsMaterial extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'material_id' => true,
        'is_material_uncertain' => true,
        'material_color_id' => true,
        'material_aspect_id' => true,
        'artifact' => true,
        'material' => true,
        'material_color' => true,
        'material_aspect' => true
    ];
}
